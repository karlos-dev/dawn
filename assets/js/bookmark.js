/*
  bookmark.js
  A crispy bookmark section for ghost themes
  Adds a font-awesome bookmart at the top header of the post area.

  Needs custom.css file to be in the project theme/assets too. 
  Version: 0.6

  0.1
  ---
  + Add cookie support
  + Add cookie monster object for bookmarks management
  + Add bookmarkSection helper

  Changes 0.2
  -----------

  + Add sharer method for social
  + Add cookieMonster as base cookie class
  + Add bookmark feature as BC
  + Add View feature ViewC

  Changes 0.3
  -----------
  + Add About hash scroll
  + Add flickr js feature
  + Add default events

  Changes 0.4
  -----------
  + Add human checks for contact form

  Changes 0.5
  -----------
  + Add visits info to live posts

  Changes 0.6
  -----------
  + Add updates from article feed site
  + Add solr related articles feature
 */

; (function($) {
  var 
    wlh=defaults.currentAddress,
    hbs_handler='i[data-elem="book-mark"]',
    s_handler='img.share',
    view_handler='.settings .icons a',
    top='#top',
    myheader='.k-nav .icons',
    imgRefresh='.new-image',
    defaultImage = defaults.defaultBgImage,
    defaultImageSrv,
    __post
    ;

  defaultImageSrv = defaults.getSrc();

  var bookmarkSection = {
    e: $('<section class="bookmark-box"><h2 class="title">Bookmarks</h2><div class="mini-posts"></div></section>'),
    s: '.bookmark-box',
    create: function(){
      if($(this.s).length == 0)
        $(this.e).insertAfter('#sidebar .search')
    },
    load: function(cm){
      var to = $(this.s), ls=cm.list();
      to.html("");
      if(ls.length > 0) {
        ls.map(function(e, i){
          // console.log(e);
          var html='<div class="bm"><span class="date"><small>'+ (new Date(e.add)).toDateString() +'</small></span></br><a class="ln" href="'+e.url+'">'+e.name+'</a></div>';
          to.append(html);
        })
      } else {
        var html='<span>You added no bookmarks yet</span>';
        to.append(html);
      }
    }
  };

  var imgActions = {
    start: '<ul class="icons">',
    buttons: { 
      'refresh': '<li><a rel="noopener" title="Refresh header image" class="icon"><i class="fa fa-refresh new-image"></i></a></li>',
      'link': '<li><a target="_blank"><i class="fa fa-external-link"></i></a></li>'
    },
    end: '</ul>',

    get: function(buttonOpts) {
      // only li items now
      var s = "" // this.start

      for(var i in buttonOpts){
        switch(buttonOpts[i]){ 
          case 'refresh': {
            s += this.buttons[buttonOpts[i]]
          } break;
          case 'link': {
            s += this.start
            s += this.buttons[buttonOpts[i]]
            s += this.end
          } break;
        }
      }
      // s += this.end;
      return s;
    } 
  };

  // request content Index service
  const _some = "signature-1841414c9d59cc1b0ace09a0d1eb9844",
        _than = "a39730b7d46d6c38f1f28c832ea18e12"
        _local = defaults.msv.indexOf('localhost') > -1
        _s = _local ? "" : "s"
        relatedCount = 5,
        mightBeInterestedCount = 2
        ;

  if(typeof(__post_id) === typeof("string")) {
    __post = _local ? "5c886fcd90af89716d4779ed" : __post_id
    $.ajax({
      url: "ht" + "tp" + _s + "://" + defaults.msv + "/related/" + __post,
      method: "GET",
      dataType: 'json',
      beforeSend: function(r) {
        r.setRequestHeader("X-Load", `S ${_some} ${_than}`);
      },
      success: function(res) {
        $('.related-feed').html("")
        
        var elem
        for(var i in res.docs) {
          if(i < relatedCount) {
            elem = generateRelated(parseInt(i, 10) + 1, res.docs[i])
            // $('.latest_style_3[data-type="related"]').append(elem)
            $('.related-feed').append(elem)
          } else {
            // @deprecated
            // elem = generateMightBe(parseInt(i, 10) + 1, res.docs[i])
            // $('.latest_style_2[data-type="might-be"]').append(elem)
          }
        }
      },
      error: function(err) {}
    })
  }

  $.ajax({
    url: "ht" + "tp" + _s + "://" + defaults.msv + "/updates",
    method: "GET",
    dataType: 'json',
    beforeSend: function(r) {
      r.setRequestHeader("X-Load", `S ${_some} ${_than}`);
    },
    success: function(res) {
      var news = generateNews(res)
      var code = generateNewsCode(news)
      $('.the' + 'ia' + 'StickySidebar').prepend(code)
    },
    error: function(err) {}
  })

  /**
   * Embed news into display box, return a jquery component
   * @param {Array} news 
   */
  const generateNewsCode = function(news) {
    var nnc = '<div class="news-widget animate-box fadeInUp animated-fast">' + 
      '<div class="widget-title-cover"><img src="/content/images/2019/06/bird.png" style="vertical-align:middle;height:20px;" /><h4 class="widget-title not"><span>Updates</span></h4></div>';
    for(var i in news) {
      nnc += news[i]
    }
    nnc += '</div>'
    return $(nnc)
  }
  /**
   * Build a TAF internal news html
   * @param {Object} d  Fetch new object
   */
  const generateNews = function(d) {
    var news = [], code, options = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric' };
    for(var i in d){
      code = '<div class="latest_style_1">' +
              '<div class="latest_style_1_item" data-uri="/moringa-the-miracle-tree/">' +
              '<div class="alith_post_title_small" style="width:100%;color:#000;padding-bottom:10px;">' +
                '<strong>' + d[i].title + '</strong>' + 
              '</div>';
      if(d[i].categories) {
        code += '<div class="meta_categories" style="width:100%;color:#000">'
        for(var j in d[i].categories) {
          code += '<a href="/tag/'+ d[i].categories[j].replace(/ /g, '-').toLowerCase() + '"><span class="post_meta_categories_label">' + d[i].categories[j] + '</span></a>'
        }
        code += '</div>'
      }
      if(d[i].comments) {
        code += '<div class="comment" style="width:100%; color:#000">'
        for(var j in d[i].comments) {
          code += '<div class="">' + d[i].comments[j] + '</div>'
        }
        code += '</div>'
      }
      code += '<div class="alith_post_title_small" style="width:100%;color:#000">' +
                '<p class="meta"><span>' + (new Date(d[i].ts * 1000)).toLocaleDateString("en-US", options).slice(5,) + '</span></p>' +
              '</div>' + 
            '</div>' +
          '</div>'
      
      news.push(code)
    }

    return news
  }
  /**
   * Generate html code for related articles
   * @param {Number} i  Current index
   * @param {Object} p  Fetch related article data
   */
  const generateRelated = function(i, p) {
    let d = new Date(p.published), y, M, day, h, m;
    y = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
    M = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
    day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
    h = new Intl.DateTimeFormat('en-US', { hour: 'numeric', hour12: false }).format(d);
    m = new Intl.DateTimeFormat('en-US', { minute: '2-digit' }).format(d);

    var code = `<article class="feed post">
      <div class="feed-options">
        <span class="item-count">${i}.</span>
      </div>
      <div class="feed-calendar">
          <div class="feed-calendar-month">
              ${M} ${day}
          </div>
      </div>
      <a class="feed-title" href="${p.origin_url}" aria-label="${p.title}"><h2 class="feed-title">${p.title}</h2></a>
      <div class="feed-right">
          <a class="" href="${p.origin_url}" aria-label="${p.title}"><div class="feed-length">
              6 min read
          </div></a>
      </div>
      <svg class="icon feed-icon">
          <use xlink:href="#chevron-right"></use>
      </svg>
    </article>`
    return code
  }
  /**
   * Generarte popup YouMightBeInterestedIn articles
   * @param {Number} i  Current index
   * @param {Object} p  Fetch related article data
   */
  const generateMightBe = function(i, p) {
    var code = '<div class="latest_style_2_item">' + 
      '<figure class="alith_news_img">';
    
    code += `<a href="${p.origin_url}">`
    code += `<img class="hover_grey" src="${p.image}" alt="${p.title}">`
    code += '</a>' + '</figure>'
    code += `<h3 class="alith_post_title"><a href="${p.origin_url}">${p.title}</a></h3>`
    code += '</div>'

    return code
  }

  class cookieMonster {

    constructor(name) {
      this.c = null;
      this.dur = { expires: 365 };
      this.name = name
    }
    get(){
      var c=Cookies.get(this.name);
      return c ? JSON.parse(c) : null
    }
    save(){
      Cookies.set(this.name, JSON.stringify(this.c), this.dur);
    }
  };

  class BC extends cookieMonster {

    check(){
      var cookie = super.get(), li, i;
      if(!cookie){
        this.c = { created: Date.now(),links: [] }
        super.save();
      } else {
        this.c = cookie
        const shown = $(hbs_handler)
        for(i in this.c.links) {
          li = this.c.links[i]
          // ops, o(n)²
          $.each(shown, (i, e) => {
            if(li.url === $(e).attr('data-url')) {
              $(e).addClass('active')
              $(e).attr('title', "Bookmarked")
            }
          })
        }
      }
    }

    delete(){
      $('.bookmarks-box').html("")
    }

    render(){
      var cookie = super.get(), li, i, text, y, M, day, h, m, title;
      if(!cookie){
        this.c = { created: Date.now(),links: [] }
        super.save();
      } else {
        this.c = cookie
        for(i in this.c.links) {
          li = this.c.links[i]
          console.log("bookmark", li)
          let d = new Date(li.add);
          y = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
          M = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
          day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
          h = new Intl.DateTimeFormat('en-US', { hour: 'numeric', hour12: false }).format(d);
          m = new Intl.DateTimeFormat('en-US', { minute: '2-digit' }).format(d);

          title = li.url.replace(/-/g, " ").replace(/\//g, "")
          title = title[0].toUpperCase() + title.slice(1)

          text = `<article class="feed post tag-health tag-hash-original">
            <div class="feed-calendar">${M} ${day}, ${y} ${h}:${m}</div>
            <div class="feed-options">
              <i data-url="${li.url}" class="fa fa-bookmark-o bookmark active" title="Bookmarked" data-elem="book-mark"></i>
            </div>
            <a class="feed-title" href="${li.url}" aria-label="${title}"><h2 class="feed-title">${title}</h2></a>
          </article>`

          $('.bookmarks-box').append(text)
        }

        if(this.c.links.length === 0) {
          text = `<article class="feed post tag-health tag-hash-original">
            <a class="feed-title" href="#" aria-label=""><h2 class="feed-title">No bookmarked articles</h2></a>
          </article>`
          $('.bookmarks-box').append(text)
        }
      }
    }

    remove(link){
      var li;
      for(var i in this.c.links) {
        li=this.c.links.shift()
        if(li.url != link)
          this.c.links.push(li)
      }
      //console.log(JSON.stringify(this.c))
      super.save();
    }

    save(link){
      this.c.links.push({'add':Date.now(),'url':link,'name':$('.single-header h3').html()})
      // console.log(JSON.stringify(this.c))
      super.save();
      // Cookies.set(this.name, JSON.stringify(this.c), this.dur);
      // console.log(this.c)
    }

    list(){
      return this.c.links;
    }
  };

  class ViewC extends cookieMonster {

    check(){
      var cookie=super.get();
      if(!cookie){
        this.c={ 'status': 'grid' }
        Cookies.set(this.name, JSON.stringify(this.c), this.dur);
      } else {
        this.c = cookie;
        //console.log(this.c);
        post_class=this.c.status == 'list' ? 'k' : '' ;
        this.update_view(post_class=='k');
      }
    }

    save(status){
      this.c={ 'status':status }
      super.save()
      // console.log(this.c)
    }

    update_view() {
      if(super.get().status == 'list') {
        $('.posts article:not(.small)').addClass('k');
        post_class='k';
        $('.settings .icons a[data-elem="view-list"]').addClass("active");
        $('.settings .icons a[data-elem="view-grid"]').removeClass("active");
      } else {
        post_class='';
        $('.posts article.k').removeClass('k');
        $('.settings .icons a[data-elem="view-grid"]').addClass("active");
        $('.settings .icons a[data-elem="view-list"]').removeClass("active");
      }
    }
  };

  class ImgC extends cookieMonster {

    loadImg(url) {
      var _this = this
      $.ajax({
        url: url, 
        method: 'GET',
        beforeSend: function(req) {
          req.setRequestHeader('Content-type', 'application/json');
        },
        success: function(data) {
          _this.back(data)
        },
        error: function(xhr, statusText, err) {
          _this.back({images: []})
        }
      })
    }

    check(force){
      var cookie = super.get();
      var _this = this,
          url = defaultImageSrv
          ;

      if(!cookie){
        this.loadImg(url)
      } else {
        this.c = cookie;
        if(force || this.c.ts + 3600 < new Date().getTime() / 1000) {
          // console.log("doing request imgs")
          this.loadImg(url)
        } else {
          // console.log("update view")
          this.update_view()
        }
      }
    }

    save(status){
      this.c=status
      super.save()
    }

    back(data) {
      this.save({'images': data.images, 'ts': new Date().getTime() / 1000})
      this.update_view()
    }

    update_view() {
      var _this = this,
          callbackRow = {},
          bgImg;

      _this.callbackCounter = 0
      for(var i=0; i< _this.c.images.length; i++) {
        bgImg = new Image()
        _this.callbackCounter += 1
        callbackRow[_this.c.images[i].url] = bgImg
        callbackRow[_this.c.images[i].url].meta = _this.c.images[i]
        callbackRow[_this.c.images[i].url].src = "data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=";
        callbackRow[_this.c.images[i].url]['data-src'] = _this.c.images[i].url;
        callbackRow[_this.c.images[i].url].onload = function(e){
          var localImg = callbackRow[e.target['data-src']],
              obj = $(".picture-grid li:nth-child(" + _this.callbackCounter + ") .img-frame")
              ;
            obj.css({"background-image": "url(" + localImg['data-src'] + "?t="+_this.c.ts+")"})
            obj.attr('title', localImg.meta.title + ', ' + localImg.meta.author)
            obj.attr('href', localImg.meta.flickr_url)

          _this.callbackCounter -= 1
        }
      }
    }
  };

  class ContribC extends cookieMonster {

    check(){
      var cookie=super.get();
      if(!cookie){
        this.c={ 'contrib': { 'side': [], 'bottom': [], 'inline': [] } }
        Cookies.set(this.name, JSON.stringify(this.c), this.dur);
      } else {
        this.c = cookie;
        // console.log(this.c);
        this.update_view();
      }
    }

    addSide() {
      var sarr = this.c.contrib.side
      sarr.push({
        at: Date.now()
      })
      this.c.contrib.side = sarr
    }

    addInline() {
      var sarr = this.c.contrib.inline
      sarr.push({
        at: Date.now()
      })
      this.c.contrib.inline = sarr
    }

    addBottom() {
      var sarr = this.c.contrib.bottom
      sarr.push({
        at: Date.now()
      })
      this.c.contrib.bottom = sarr
    }

    save(){
      // this.c={ 'contrib': contrib }
      super.save()
      // console.log(this.c)
    }

    update_view() {
      var i
      for(i in this.c.contrib.side) {
        if( (new Date(this.c.contrib.side[i].at)).toLocaleDateString() === (new Date()).toLocaleDateString()) {
          setTimeout(function(){
            // console.log("contributed today")
            $('.sidebar_right .side-ad').parent().remove()
          }, 1000)
        }
      }
      for(i in this.c.contrib.inline) {
        if( (new Date(this.c.contrib.inline[i].at)).toLocaleDateString() === (new Date()).toLocaleDateString()) {
          setTimeout(function(){
            // console.log("contributed today")
            $('.inline-ad').parent().remove()
          }, 1000)
        }
      }
      for(var k in this.c.contrib.bottom) {
        if((new Date(this.c.contrib.bottom[k].at)).toLocaleDateString() === (new Date()).toLocaleDateString()) {
          setTimeout(function(){
            // console.log("contributed today")
            $('.bottom .side-ad').parent().remove()
          }, 1000)
        }
      }
    }
  };

  var sharer = function(){
    var 
      ep_fb = "https://www.facebook.com/share.php",
      ep_tw = "https://twitter.com/intent/tweet",
      fb_url = ep_fb + '?u=' + window.location,
      tw_url = ep_tw + '?url=' + window.location+'&text=' + $('h1').html().replace(/ /g, '+');

    $('a.fa-facebook').attr('href', fb_url);
    $('a.fa-twitter').attr('href', tw_url);

    $('a i.fa-facebook').parent().attr('href', fb_url);
    $('a i.fa-twitter').parent().attr('href', tw_url);
  }

  var awakeFrames = function(){
    $.each($('.img-frame'), function(i, e){
      var width = $(e).width()
      $(e).css({height: width + "px"})
    })
  }

  // Contact form
  var isFormEnabled = false,
      uiLog = [];

  $(window).on('mousemove', function(){
    if(typeof(__contact) !== typeof(undefined)) {
      if(uiLog.indexOf('nameFocus') === -1) {
        uiLog.push('moveOnContact')
      }
    }
  })
  
  $('.comment-form input[name=author]').on('focus', function(e){
    if(uiLog.indexOf('nameFocus') === -1) {
      uiLog.push('nameFocus')
    }
  })

  $('.comment-form input[name=email]').on('focus', function(e){
    if(uiLog.indexOf('emailFocus') === -1) {
      uiLog.push('emailFocus')
    }
  })

  $('.comment-form textarea[name=comment]').on('focus', function(e){
    if(uiLog.indexOf('textareaFocus') === -1) {
      uiLog.push('textareaFocus')
    }
  })

  $('.comment-form input[type=submit]').on('click', function(e){
    e.preventDefault()

    var all = [], reg = ['moveOnContact', 'nameFocus', 'emailFocus', 'textareaFocus']
    for(var i in reg) {
      all.push(uiLog.indexOf(reg[i]) !== -1)
    }
    if(all.every(Boolean)) {
      // console.log("sending...")

      var data = {
          author: $('input[name=author]').val(),
          email: $('input[name=email]').val(),
          comment: $('textarea[name=comment]').val()
        }, 
        button = $('input[type=submit]'),
        fallback = function() {
          $('.form-submit i').remove()
          button.css({'padding-right': '25px'})
        },
        mySuccessCallback = function(res) {
          fallback()
          button.val("Sent")
          $('<div class="notice">Message sent. We\'ve sent a copy to your email address <u>' + data.email + '</u></div>').insertAfter(button)
        },
        myErrorCallback = function(res) {
          fallback()
          button.val("Send Message")
          $('<div class="notice">There was an error sending the email. Please try again.</div>').insertAfter(button)
        };

      // Update send button
      button.val("Sending")
      button.css({'padding-right': '35px'})
      $('<i class="fa fa-spin fa-refresh"></i>').insertAfter(button)

      $.ajax( {
        url: defaults.mailerAddr + '/deliver/email',
        method: 'POST',
        data: $.param(data),
        success: mySuccessCallback,
        error: myErrorCallback
        //{ headers: { 'Content-t': state.register.csfr } } 
      });
    }
  })

  // load classes
  var 
    $window = $(window),
    bcm = new BC('tafbmkc'),
    vcm = new ViewC('tafvc'),
    icm = new ImgC('imgc'),
    contrib = new ContribC('contrib'),
    elems
    ;

  // startup checks
  icm.check();
  contrib.check();
  bookmarkSection.create();
  vcm.check();
  // sharer();
  awakeFrames();

  // liveposts visits ratio
  const ratio = 3;

  /**
   * updateTop
   * (however, on Dawn theme, updated section is at the bottom)
   * Update the articles shown in the most visited slider
   */
  const updateTop = function(arr, url, views){
    var elem,
        vint = parseInt(views, 10) * ratio,
        fixedUrl = url.replace(/\//g, '')
        ;
    $.each(arr, function(i, e){
      // console.log("item in owl", e, i, fixedUrl)
      if(!$(e).attr('data-uri'))
        return
      if($(e).attr('data-uri').indexOf(fixedUrl) > -1) {
        elem = $(e).find('.view-count')
        elem.html(vint.toString() + " views")
      }
    })
  }
  /**
   * loadVisits
   * load liveposts visits to update their view counts
   */
  const loadVisits = function(){
    var elemsTop = $.find('.owl.visited article'),
        data,
        i,
        sorted
        ;
    if(typeof __visits !== typeof undefined) {
      data = JSON.parse(__visits)
      sorted = {}
      // console.log("visits >>")
      for(i in data) {
        sorted[data[i].views] = data[i]
        updateTop(elemsTop, data[i].url, data[i].views)
      }
    }
  }
  /**
   * reduceAds
   * ported from inlinead, update tags when mobile browsing
   * This is a safe measure as scripts are not embedded if the
   * screen width doesn't match
   */
  const reduceAds = function() {
    if(window.innerWidth <= 500) {
      $('.inline-ad.hidden-xs').remove()
    } else {
      $('.inline-ad.hidden-sm.hidden-md.hidden-lg').remove()
    }
    $('.ad-box').css({display:'block'})
  }
  

  // ga event declaration
  var 
    refresh = {
      category: 'interaction',
      action: 'click',
      label: 'refreshImage',
      value: 1
    },
    bookmark = {
      category: 'interaction',
      action: 'click',
      label: 'bookmark',
      value: 1
    },
    click
    ;

  // click handlers
  $(top).on('click', imgRefresh, function(e){
    $(imgRefresh).addClass("fa-spin")
    __ga.sendEvent(refresh.category, refresh.action, refresh.label, refresh.value); 
    icm.check(true)
  })

  $('.tr-c a').on('click', function(){
    click = {
      category: 'visit',
      action: 'click',
      label: 'trend',
      value: 1
    }
    __ga.sendEvent(click.category, click.action, click.label, click.value); 
  })

  $('#ghost-search-results a').on('click', function(){
    click = {
      category: 'visit',
      action: 'click',
      label: 'search-result',
      value: 1
    }
    __ga.sendEvent(click.category, click.action, click.label, click.value); 
  })

  $(hbs_handler).on('click', function(e){

    var url = $(e.target).attr('data-url')
    if (!url) {
      console.warn("missing url in bookmark")
      return
    }
    if( $(e.target).hasClass("active") ){
      bcm.remove(url);
      bookmarkSection.load(bcm);
      $(e.target).removeClass("active");
      $(e.target).attr('title','Save in your collection');
    }
    else {
      __ga.sendEvent(bookmark.category, bookmark.action, bookmark.label, bookmark.value);
      bcm.save(url);
      bookmarkSection.load(bcm);
      $(e.target).addClass("active");
      $(e.target).attr('title','Saved');
    }
  })

  $(s_handler).on('click', function(e){
    if($(e.target).hasClass('rotate90'))
      $('.layer').animate({left: '0px', opacity: 0.5}, 300, function(){ $('.layer').hide() }) //.hide();
    else
      $('.layer').show().animate({left: '-45px', opacity: 1}, 300);
    $(e.target).toggleClass('rotate90')
  })

  $('.inline-ad').on('mouseenter', function(e){
    
  })

  $('.inline-ad').on('mouseleave', function(e){
    
  })

  $('.bookmarks').on('click', function(e){
    console.log(e)

    // var i = $(e.target).find('i')

    if(!$(e.target).hasClass('active')) {
      console.log("has class")
      $('.bookmarks-section').show()
      // $(e.target).removeClass('active')
      $(e.target).addClass('active')
      bcm.render();
    } else {
      console.log("hasn't class")
      $(e.target).removeClass('active')
      // $(e.target).addClass('fa-bookmark-o')
      $('.bookmarks-section').hide()
      bcm.delete();
    }
  })

  /**
   * Super img error handler
   */
  $('img').on('error', (e) => {
    var image = e.target, total;

    if($(image).attr('fetch-total')) {
      total = parseInt($(image).attr('fetch-total'), 10)
      total += 1
      $(image).attr('fetch-total', total)
      if(total === 3) {
        console.log(`Reach max attemtps`)
        return
      }
    } else {
      $(image).attr('fetch-total', 1)
    }

    if($(image).attr('fetch')) {
      image.src = '/assets/images/no-image-750x460.jpg'
      console.log(`Set default no-image`)
      return
    }

    // console.log(image.src)
    const imageUri = `${defaults.imageServer}/image/fetch?q=${encodeURIComponent(image.src)}`
    console.log(`Fetching ${imageUri}`)
    image.src = imageUri

    // reset interface pretty whatever
    $(image).removeClass('lazyautosizes')
    $(image).removeClass('lazyloaded')

    $(image).removeAttr('data-srcset')
    $(image).removeAttr('data-sizes')
    $(image).removeAttr('srcset')

    $(image).attr('fetch', Date.now())
  })
  
  // onload completion
  $window.on('load', function() {
    console.log("onload")
    __ga = new GaSuite();
    elems = $('a[data-elem="book-mark"]');
    bcm.check();
    bookmarkSection.load(bcm);
    $(myheader).append(imgActions.get(['refresh']))
    loadVisits()
    reduceAds()
  });

  // remove social links from o.f.f.g.u.a.r.d.i.a.n articles
  $.each($('article a'), (i, e) => {
    if($(e).attr('class') && $(e).attr('class').indexOf('socicon') > -1) {
      $(e).remove()
    }
  })
  // remove o.f.f.g.u.a.r.d.i.a.n support iframe
  $.each($('h2'), (i, e) => {
    if($(e).html().startsWith("SUPPORT OFFGUARDIAN")) {
      let iframeSib = $(e).next('iframe')
      let nextp = $(iframeSib).next('p')
      $(iframeSib).remove()
      $(nextp).remove()
      $(e).remove()
    }
  })

  // remove e.x.p.o.s.e. support blocks
  let exposeClasses = [
    '.has-text-align-left.has-arial-font-family.has-custom-font',
    '.has-text-align-center.has-arial-font-family.has-custom-font'
  ]
  let exposeIds = ['#subscribe-email', '#subscribe-submit']
  exposeClasses.map(e => {
    $(e).remove()
  })
  exposeIds.map(e => {
    $(`p${e}`).remove()
  })

  // scroll to header
  if(wlh.indexOf('about') !== -1 || wlh.indexOf('terms') !== -1 ) {
    var 
      str = window.location.href.substring(window.location.href.indexOf('#') + 1),
      elem = $("*[name='"+str.toLowerCase()+"']")[0]
      ;
    if(!elem) {}
    else {  
      $('html, body').animate({
        scrollTop: $(elem).offset().top
      }, {duration: 1000, easing: 'linear'});
    }
  }

})(jQuery);
