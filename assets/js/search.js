/**
 * The article feed search
 * v1.0.1
 * 
 * Dynamic implementation for terms search.
 * terms can be single: search text
 *           or quoted: "search text"
 * then they can be OR (any) or AND (all)
 */
; (function($) {
    // left offset from question circle button
    // top offset from question circle button
    
    var searchMode = 'or',
        posX,
        offsetX = 10,
        offsetY = 40,
        isOpen = false,
        gSearchField = $('#ghost-search-field'),
        gSearchResults = $('#ghost-search-results'),
        gSearchHeader = $('.ghost-search-header'),
        tools = $('.tools'),
        helpBox = $('.help-box'),
        isQuoted = $('.is-quoted'),
        qCircle = $('.tools .fa-question-circle')
        ;
    
    var searchText = "";

    gSearchField.on('focus', function(e) {
        tools.slideDown()
    })

    gSearchField.on('blur', function(e) {})

    $('.tools .help').on('click', function(e){
        // `offset()` is useful after a `.on('load')` handler
        // I assume, the click will be done once the page is loaded
        posX = qCircle.offset()
        
        helpBox.css({left: `${posX.left - offsetX}px`, top: `${posX.top + offsetY}px`})
        helpBox.toggle()
        qCircle.toggleClass("on")
    })

    $('button[data-modal=search]').on('click', function(e){
        // if(!$(e.target).hasClass('fa-close'))
        //  return;
        isOpen ? $('.search-bar').hide() : $('.search-bar').show()
        isOpen ? $(this).removeClass('active') : $(this).addClass('active')
        
        isOpen = !isOpen
        tools.slideUp()
        gSearchField.val("")
        gSearchField.focus()
        qCircle.removeClass("on")
        helpBox.hide()
        renderQuoteInfo(searchText)
        clearResults()
    })

    $('.search-bar i:not(.fa-refresh)').on('click', function(e){
        if(!$(e.target).hasClass('fa-close'))
          return;
        
        searchText = ""
        gSearchField.val("")
        qCircle.removeClass("on")
        renderQuoteInfo(searchText)
        clearResults()
        gSearchField.focus()
    })

    $('input[name=relate]').on('click', function(){
        searchMode = $(this).attr('data-value').toLowerCase()
        renderModeInfo(searchMode)
    })

    const renderQuoteInfo = (searchText) => {
        console.log("quote render", searchText)
        // if(typeof searchText === typeof undefined)
        //    var searchText = ""
        if(searchText.length > 1 && searchText[0] === '"' && searchText[searchText.length-1] === '"')
            $('.is-quoted').html("quoted")
        else
            $('.is-quoted').html("")
    }

    const renderModeInfo = () => {
        $('.mode-box').html(`<b>${searchMode == 'or' ? "ANY" : "ALL"}</b> of the entered words would match`)
    }

    const renderResult = (result) => {
        var url = [location.protocol, '//', location.host].join('');
        var datestring = (new Date(result.published)).toDateString();
        var html = '<article class="row">'
        html += '<div class="col-md-4">'
        html += '<figure class=""><a href="' + url + result.origin_url + '">'
        html += '<img src="' + result.image + '" alt=""></a></figure>'
        html += '</div>'
        html += '<div class="col-md-8 result-text">'
        html += '<h3 style="margin: 0 0 .5rem;"><a href="' + url + result.origin_url + '">' + result.title + '</a></h3>'
        html += '<div class="">'
        html += '<span class=""><small>' + datestring + '</small></span>'
        html += '</div>'
        if(result.excerpt && result.excerpt !== 'no description')
            html += '<p class="alith_post_except">' + result.excerpt + '</p><br>';
        html += '<a href="' + url + result.origin_url + '" class="read-more">Read More</a>'
        html += '</div>';
        html += '</article>';
        return(html);
    }

    const appendResults = (res) => {
        gSearchResults.append(res)
    }

    const clearResults = (res) => {
        gSearchHeader.hide()
        gSearchResults.hide().html("").show()
    }

    const mySuccessCallback = (res) => {
        gSearchHeader.show();
        var docsArr = res.docs
        for(var i in docsArr) {
            appendResults(renderResult(docsArr[i]))
        }
        stopLoading()
    }
    
    const myErrorCallback = (res, status, err) => {
        console.warn(res, status)
        stopLoading()
    }

    const search = (terms, mode) => {
        var qs = terms.map(e => e.trim()).join('+'),
            ms = (mode && mode === 'and') ? 'and' : 'or',
            xx = Math.random().toString(13).replace('0.', ''),
            xy = Math.random().toString(13).replace('0.', '')
            ;

        $.ajax({
            url: `${location.protocol}//${defaults.msv}/search?q=${qs}&m=${ms}`,
            method: 'GET',
            success: mySuccessCallback,
            error: myErrorCallback,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Content-Type", "application/json")
                xhr.setRequestHeader("X-Load", `S ${xx} ${xy} ${Date.now()}`)
            }
        });
    }

    const setLoading = () => {
        $('.search-bar i').addClass('fa-refresh fa-spin').removeClass('fa-close')
    }

    const stopLoading = () => {
        $('.search-bar i').addClass('fa-close').removeClass('fa-refresh fa-spin')
    }

    gSearchField.on('keyup', function(e) {
        searchText = $(this).val()
        console.log(searchText)
        switch(e.keyCode){ 
            case 13: {
                clearResults()
                var stxt
                if(searchText[0] === '"' && searchText[searchText.length-1] === '"') {
                    stxt = [searchText.trim().replace(/"/g, '').replace(/ /g, '_')]
                } else {
                    stxt = searchText.split(" ")
                }
                setLoading()
                search(stxt, searchMode)
            } break
            case 77: {
            } break
            default: {
            }
        }
        renderQuoteInfo(searchText)
    })

    /**
     * Initialize all
     */
    renderModeInfo();
    renderQuoteInfo(searchText);

    if(gSearchField.val() !== "") {
        tools.slideDown()
        searchText = gSearchField.val()
        renderQuoteInfo(searchText)
    }

})(jQuery);
